import java.util.Scanner;

public class TicTacToeGame
{
	public static void main(String[] args)
	{
		// Square x = Square.X;
		// Square o = Square.O;
		// Square blank = Square.BLANK;
		
		// System.out.println(x);
		// System.out.println(o);
		// System.out.println(blank);
		
		System.out.println("Welcome to my game of Tic Tac Toe!");
		
		Board board = new Board();
		Scanner scan = new Scanner(System.in);
		
		boolean gameOver = false;
		int player = 1;
		Square playerToken = Square.X;
		
		while(!gameOver)
		{
			System.out.println(board);
			
			if(player == 1)
				playerToken = Square.X;
			else
				playerToken = Square.O;
			
			int row = -1;
			int col = -1;
			do
			{
				System.out.println("Which row would you like to place your token(0, 1, or 2)?");
				row = scan.nextInt();
				System.out.println("Which column would you like to place your token(0, 1, or 2)?");
				col = scan.nextInt();
			}
			while(!board.placeToken(row, col, playerToken));
			
			if(board.checkIfWinning(playerToken))
			{
				System.out.println("Player " + player + " wins!");
				System.out.println(board);
				gameOver = true;
			}
			else if(board.checkIfFull())
			{
				System.out.println("Its a tie!");
				System.out.println(board);
				gameOver = true;
			}
			
			player++;
			if(player > 2)
				player = 1;
		}			
	}
}