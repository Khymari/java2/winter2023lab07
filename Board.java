public class Board
{
	private Square[][] tictactoeBoard;
	
	private Square x = Square.X;
	private Square o = Square.O;
	private Square b = Square.BLANK;
	
	public Board()
	{
		tictactoeBoard = new Square[][]
		{	{b,b,b},
			{b,b,b},
			{b,b,b}
		};
	}
	
	public boolean placeToken(int row, int col, Square playerToken)
	{
		if(isRowColValid(row, col))
		{
			if(tictactoeBoard[row][col] == b)
			{
				tictactoeBoard[row][col] = playerToken;
				return true;
			}
			else 
				return false;
		}
		else 
			return false;
	}
	
	public boolean checkIfFull()
	{
		for(int i = 0; i < tictactoeBoard.length; i++)
		{
			for(int j = 0; j < tictactoeBoard[i].length; j++)
			{
				if(tictactoeBoard[i][j] == b)
					return false;
			}
		}
		return true;
	}
	
	public boolean checkIfWinning(Square playerToken)
	{
		if(checkIfWinningHorizontal(playerToken) || checkIfWinningVertical(playerToken))
			return true;
		else
			return false;
	}
	
	private boolean checkIfWinningHorizontal(Square playerToken)
	{
		for(int i = 0; i < tictactoeBoard.length; i++)
		{
			int count = 0;
			for(int j = 0; j < tictactoeBoard[i].length; j++)
			{
				if(tictactoeBoard[i][j] == playerToken)
					count++;
			}
			if(count == 3)
				return true;
		}
		return false;
	}
	
	private boolean checkIfWinningVertical(Square playerToken)
	{
		for(int i = 0; i < tictactoeBoard.length; i++)
		{
			int count = 0;
			for(int j = 0; j < tictactoeBoard[i].length; j++)
			{
				if(tictactoeBoard[j][i] == playerToken)
					count++;
			}
			if(count == 3)
				return true;
		}
		return false;
	}

	private boolean isRowColValid(int row, int col)
	{
		if((row >= 0 && row <= 2) && (col >= 0 && col <= 2))
			return true;
		else
			return false;
	}
	public String toString()
	{
		String board = "";
		for(int i = 0; i < tictactoeBoard.length; i++)
		{
			board += ("|");
			for(int j = 0; j < tictactoeBoard[i].length; j++)
			{
				board += tictactoeBoard[i][j];
			}
			board += ("|");
			board += ("\n");
		}
		return board;
	}
}